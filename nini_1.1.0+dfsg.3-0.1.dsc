-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: nini
Binary: libnini1.1-cil, libnini-cil-dev, libnini-doc
Architecture: all
Version: 1.1.0+dfsg.3-0.1
Maintainer: Debian CLI Libraries Team <pkg-cli-libs-team@lists.alioth.debian.org>
Uploaders: Sebastian Dröge <slomo@debian.org>, Mirco Bauer <meebey@debian.org>
Homepage: http://nini.sourceforge.net
Standards-Version: 3.8.3
Build-Depends: debhelper (>= 7.0.50~), cli-common-dev (>= 0.5.7)
Build-Depends-Indep: mono-devel (>= 2.4.2.3), sharutils
Package-List:
 libnini-cil-dev deb devel optional arch=all
 libnini-doc deb doc optional arch=all
 libnini1.1-cil deb devel optional arch=all
Checksums-Sha1:
 045040894c263f4347e2234d202efa04780ac1f6 68820 nini_1.1.0+dfsg.3.orig.tar.xz
 41a449ba86ef50cd13edacbc0e026e77599fedc8 4552 nini_1.1.0+dfsg.3-0.1.debian.tar.xz
Checksums-Sha256:
 b7dedbb92db318d8668718d44b17fc87caa2e01ac74b9aa9bd3f8b42cd0d37b7 68820 nini_1.1.0+dfsg.3.orig.tar.xz
 5eb3ad96859e6c1b03da47a52a02d06d69c3a76a67796733777fcc11be536140 4552 nini_1.1.0+dfsg.3-0.1.debian.tar.xz
Files:
 3f08f5b5ed57105132b4d2dc8484672c 68820 nini_1.1.0+dfsg.3.orig.tar.xz
 aba024021a7c8c4686fc86fdbe82cab0 4552 nini_1.1.0+dfsg.3-0.1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHEBAEBCgAuFiEEQGIgyLhVKAI3jM5BH1x6i0VWQxQFAmZz1SwQHGJhZ2VAZGVi
aWFuLm9yZwAKCRAfXHqLRVZDFDFXDACNeK/j729f6sadw7fiof1LKI5sG4NI0CN8
j0ozLKN/AF8otuYJux554QrDORO+2ytjdu/M836JDhxmad4njbgYy99JrOUmOqJ4
jPyF/KqpduaEq+bSHC3RScqXcJCqOuPeK2Zb0T6mfXqBWLBZCVraU2vmdGVIJ3ZW
stWNshJ+6nb2rV1HqmaAyB/2GuG7UNlpmCvYggOTkBmtjbCM67ezPG0U+QQsBK0E
7aCXqzKwZWUEF4rlN+PTVpeBS+O/CqNOu25n1a1K1l6tiYTSKBeBct1T2Y39eXCb
IGLCu3gJqxQz1wpysnksVpnYrhYdVmXGk2M0IhoJoCqrEbVaseiJYiIqYYNu1T7F
XsRFvEXvFBo42uCRRFOsA3CN1gGYDifFihEKFADE5CZRAzM25aFi/hO27gPtZ+oW
HiRLZc3HLWTwop8McHma7RMmXjWY1M/zLeeSb0Ug5M5ggYwYr5LL3yNrFcB6ssMW
4ppGOaeh/Y6tWrgOjezhsB9dLolxyJA=
=u8/7
-----END PGP SIGNATURE-----
